import 'package:flutter/material.dart';
import 'app_state.dart';
import 'package:provider/provider.dart';

class Favorites extends StatelessWidget {
  const Favorites({super.key});

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    var favorites = appState.favorites;
    return ListView(
      children: [
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Text('You have ${favorites.length} favorites:'),
            ),
            for (var fav in favorites)
              ListTile(
                leading: const Icon(Icons.favorite),
                title: Text(fav.asLowerCase),
                trailing: ElevatedButton(
                    onPressed: () => appState.removeFavorite(fav),
                    child: const Icon(Icons.delete)),
              ),
          ],
        ),
      ],
    );
  }
}
